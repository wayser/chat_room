<?php
namespace app\wap\controller;
/**
 * 聊天主逻辑
 * 主要是处理 onMessage onClose 
 */
use \GatewayWorker\Lib\Gateway;
use think\Session;
class Events
{
    /**
     * 当businessWorker进程启动时触发。每个进程生命周期内都只会触发一次
     * @param  [type] $businessWorker businessWorker进程实例
     * @return [type]                 [description]
     */
    public static function onWorkerStart($businessWorker)
    {
       echo "\n\nWorkerStart -fuchao\n\n";
    }

    /**
     * [onConnect description]
     * @param  [type] $client_id [description]
     * @return [type]            [description]
     */
    public static function onConnect($client_id)
    {
       echo "connected -fuchao\n\n";
    }

    /**
     * [onMessage description]
     * @param  [type] $client_id [description]
     * @param  [type] $message   [description]
     * @return [type]            [description]
     */
    public static function onMessage($client_id, $message) {
    	// 客户端传递的是json数据
        $message_data = json_decode($message, true);
        if(!$message_data) {
            return;
        }

        switch($message_data['type']) {
            case 'pong':
                echo $message;
            break;
            case 'login':
                $cicle_arr = explode("-",$message_data['cicles']);
                foreach ($cicle_arr as $key => $value) {
                    gateway::joinGroup($client_id,$value);
                }
                Gateway::bindUid($client_id,$message_data['uid']);
                Gateway::setSession($client_id, array('uid'=>$message_data['uid'], 'relaname'=>$message_data['relaname'],'avator'=>$message_data['avator']));
                break;
                // $_SESSION['uid'] = $message_data['uid'];
                // $_SESSION['relaname'] = $message_data['relaname'];
                // $_SESSION['avator'] = $message_data['avator'];
                // $data = [
                //     "type" => "cicle",
                //     "data" => [
                //         "cicleid" => 1,
                //         "msg" => "大家好啊",
                //         "dates" => date('Y-m-d H:i'),
                //     ],
                // ];
                //gateway::sendToGroup(1,json_encode($data));
                
            case 'say':
                $user_info = Gateway::getSession($client_id);
                $datas = [
                    "type" => "say",
                    "data" => [
                                "uid" => $user_info['uid'],
                                "relaname" => $user_info['relaname'],
                                "avator" => $user_info['avator'],
                                "content" => $message_data['content'],
                                "date"  => date('Y-m-d H:i'),
                            ],
                ];
                gateway::sendToGroup(1,json_encode($datas));
                //gateway::sendToCurrentClient(json_encode($data));
            break;
        }

    }

    /**
     * 当用户断开连接时触发的方法
     * @param integer $client_id 断开连接的客户端client_id
     * @return void
    */
    public static function onClose($client_id)
    {
       echo "zen me duan kai le a\n\n";
    }

}

?>