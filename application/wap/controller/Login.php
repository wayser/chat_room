<?php
namespace app\wap\controller;
use think\Controller;
use think\Session;
use think\Db;
use think\request;

class Login
{
    public function index()
    {
        //$this->assign('name','fuchao');
        // returnJson(SUCCESS,['id'=>15445,'url'=>'www.zerofc.cn']);
        // die;
        return view('wap/Login/index');
        //return $view->fetch('wap/Index/chat_list');
    }

    public function loginEvent() {
    	if(request()->isAjax()) {
    		$username = $_POST['username'];
    		$password = $_POST['password'];
    		if(empty($username) || empty($password)) {
    			return returnJson(PARM_INSUFF);
    		}

    		$user_data = Db::name('user')->where(['username'=>$username,'status'=>1])->find();
    		if(empty($user_data)) {
    			return returnJson(LOGIN_FAIL);
    		}

    		if($user_data['password'] != md5($password)) {
    			return returnJson(USER_ERROR);
    		}

    		session::set('uid',$user_data['id']);
            session::set('relaname',$user_data['realname']);
            session::set('avator',$user_data['avator']);
    		return returnJson(SUCCESS,['uid'=>$user_data['id'],'url'=>request::instance()->domain()]);
    	}
    }

}
