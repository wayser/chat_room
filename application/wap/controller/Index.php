<?php
namespace app\wap\controller;
use think\Controller;
use think\Session;
use think\Db;

class Index extends Base
{
    public function index()
    {
        //$this->assign('name','fuchao');
        // $user_info = [
        //     "uid" => session::get('uid'),
        //     "relaname" => session::get('relaname'),
        //     "avator" => session::get('avator')
        // ];
        // $this->assign('userinfo',$user_info);

        // 获取我加入的群
        $cicle_list = Db::name('cicle_user')->alias('cu')->field('cicle.id,cicle.name,cicle.logo')
        ->where(['cu.uid'=>session::get('uid'),'cu.delete_id'=>0])
        ->join('cicle','cicle.id=cu.cicle_id and cicle.delete_id=0','left')->select();
        $this->assign('cicle_list',$cicle_list);
        return view('wap/Index/chat_list');
        //return $view->fetch('wap/Index/chat_list');
    }

    public function cicle_chat() {
    	return view('wap/Index/chat');
    }
}
